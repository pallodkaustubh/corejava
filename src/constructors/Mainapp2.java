package constructors;

class Student
{
	int student_id ;
	double student_perc;
	String student_name;


	 // user defined constructor//

     Student(int id, double perc, String name)
     {
       student_id=id;
       student_perc=perc;
       student_name=name;
     }
     void displayDetails()
     {
        System.out.println(student_id );
        System.out.println(student_perc);
        System.out.println(student_name );
        
     }
}
class Mainapp2
{
	public static void main(String[] args) 
	{
		Student s1 = new Student(101,78.5,"shyam");
		s1.displayDetails();

		Student s2 = new Student(102,88.5,"ram");
		s2.displayDetails();
	}
}