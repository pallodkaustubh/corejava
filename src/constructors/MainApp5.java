package constructors;

class central
 {
 	int k ;
 	double d ;

 	central(int k ,double d)
 	{
 		this.k=k;
 		this.d=d;

 	}
 	void display()
 	{
 		System.out.println(this);
 		System.out.println("K: "+k);
 		System.out.println("D: "+d);
 	}

 }
 class MainApp5
 {
 	public static void main(String[] args) {
 		central c1 = new central(10,30.32);
 		c1.display();
 		System.out.println(c1);
 	}
 }