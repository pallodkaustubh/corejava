package constructors;

class Registration
{
	String username;
	int contact;
	String emailid;
	String address;

	Registration(String name ,int ContactNO , String Email , String CurrentAddress)
	{
		username=name;
		contact=ContactNO;
		emailid=Email;
		address=CurrentAddress;
	}
	void Displaydetails()
	{
		System.out.println(username);
		System.out.println(contact);
		System.out.println(emailid);
		System.out.println(address);
	}

}

class MainApp3
{
	public static void main(String[] args) 
	{

		Registration r1 = new Registration("admin",12345,"admin@gmail.com","pune");

		r1.Displaydetails();


		Registration r2 = new Registration("admin1",67895,"admin1@gmail.com","mumbai");

		r2.Displaydetails();

	}
}