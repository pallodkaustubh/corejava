package constructors;

class Demo //business logic class
{  

 /*
     demo()
     {
	    //default constructor
     }
     */


	void test()
	{
		System.out.println("test method");
	}
}
class Mainapp1
{
	public static void main(String[] args) 
	{
		Demo d1 = new Demo();
		d1.test();
	}
}