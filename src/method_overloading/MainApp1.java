package method_overloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        student s1 = new student();
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("Select Serach Criteria");
        System.out.println("1: Search by name");
        System.out.println("2: Search by contact");
        int choice = Sc1.nextInt();

        if(choice==1)
        {
            System.out.println("Enter Student name");
            String Student_name = Sc1.next();
            s1.search(Student_name);
        } else if (choice==2) {
            System.out.println("Enter contact");
            int contact = Sc1.nextInt();
            s1.search(contact);

        }
    }
}
