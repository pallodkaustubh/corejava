package array;

import java.util.Scanner;

public class arraydemo14 {

        public static void main(String[] args) {
            Scanner sc1=new Scanner(System.in);
            System.out.println("ENTER TOTAL NO OF FLOORS");
            int floors= sc1.nextInt();
            System.out.println("ENTER TOTAL NO OF FLATS ON EACH FLOOR");
            int flats= sc1.nextInt();
            int[][]data=new int[floors][flats];
            System.out.println("Enter"+(floors*flats)+"Flat Nos");
            //accept flat nos
            for(int a=0;a<floors;a++){
                for(int b=0;b<flats;b++){
                    data[a][b]= sc1.nextInt();
                }
            }
            System.out.println("=========");
            for (int a=0;a<floors;a++){
                System.out.println("FLOOR NO"+(a+1));
                System.out.println("-----------");

                for (int b=0;b<flats;b++){
                    System.out.print("FLAT NO:"+data[a][b]+"\t");
                }
                System.out.println();
                System.out.println("========");
            }
        }
}
