package whileloop;

import java.util.Scanner;

public class Whileloopdemo3 {
    public static void main(String[] args) {
        Scanner sc2 = new Scanner(System.in);
        boolean status = true;
        while(status)
        {
            System.out.println("1 : Enter Addition");
            System.out.println("2 : Subtraction");
            System.out.println("3 :  Exit");
            int choice = sc2.nextInt();

            System.out.println("Enter num 1");
            int no1 = sc2.nextInt();

            System.out.println("Enter num 2");
            int no2 = sc2.nextInt();

            if(choice==1)
            {
                System.out.println(no1+no2);
            }
            else if (choice==2)
            {
                System.out.println(no1-no2);
            }
            else
            {
                status=false;
            }

        }
    }
}
