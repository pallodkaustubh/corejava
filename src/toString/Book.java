package toString;

public class Book {
    int BookId;
    String BookName;
    Double Bookprice;


    public Book(int bookid,String bookname , double price)
    {
        this.BookId=bookid;
        this.BookName=bookname;
        this.Bookprice=price;
    }

    //converting obj into string

    public String toString(){
        return  BookId+"\t"+BookName+"\t"+ Bookprice ;
    }

}
