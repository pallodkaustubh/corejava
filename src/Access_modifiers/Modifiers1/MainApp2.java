package Access_modifiers.Modifiers1;

public class MainApp2 {
    public static void main(String[] args) {
        Master m1 =new Master();
        System.out.println("B \t" +m1.b); //default
        System.out.println("C \t" +m1.c); //protected
        System.out.println("D \t" +m1.d); //public
    }
}
