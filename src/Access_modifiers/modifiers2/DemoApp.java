package Access_modifiers.modifiers2;

import Access_modifiers.Modifiers1.Master;

public class DemoApp extends Master
{
    public static void main(String[] args) {
        DemoApp d1 = new DemoApp();
        System.out.println("C \t" +d1.c);  //protcted
        System.out.println("D \t" +d1.d);   //public
    }
}
