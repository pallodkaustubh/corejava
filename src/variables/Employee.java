package variables;

class Employee
{
	public static void main(String[] args)
	{
	//declaration
	int empid;
	String empname;
	double empsalary;

	//initialization
	empid=125;
	empname="Elon";
	empsalary=35000;

	//print info
    System.out.println("employee id :"+empid);
    System.out.println("employee name :"+empname);
    System.out.println("employee salary :"+empsalary);
	}
}