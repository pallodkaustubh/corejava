package variables;

class Demo3
{
	public static void main(String[] args)
	{
		double basicsalary=12000;
		double hra =2000;
		double pf=1500;
		double incentive=1500;
		double prof_tax=3500;

		//calculate net salary

		double netsalary= basicsalary+hra+incentive-pf-prof_tax;

		//calculate gross salary

		double grosssalary=basicsalary+hra+incentive; 

		System.out.println("gross salary is : "+grosssalary);
	    System.out.println("net salary is : "+netsalary);

	}
}