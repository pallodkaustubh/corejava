package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo9 {
    public static void main(String[] args) {
        Scanner Sc1  = new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty = Sc1.nextInt();
        System.out.println("Enter Price");
        double price = Sc1.nextDouble();
         billCalculator(qty,price);
    }
    static void billCalculator(int qty , double price){
        if(qty>0 && price>0)
        {
            double total =qty*price;
            System.out.println("Total Bill :" +total);

        }
        else {
            throw  new IllegalArgumentException("Invalid Price Or Amount");
        }
    }
}
