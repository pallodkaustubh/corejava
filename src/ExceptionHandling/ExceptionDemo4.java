package ExceptionHandling;

public class ExceptionDemo4 {
    public static void main(String[] args) {
        System.out.println("Prog Started");
        int [] data = new int[3];
        String s= null;
       try {
           try {
               data[1]=50;
               data[2]=60;
               System.out.println(data[1] + "\t" + data[4]);
           }
           catch (ArrayIndexOutOfBoundsException a){
               System.out.println(a);
               System.out.println(s.toLowerCase());
           }


       }
       catch (NullPointerException b){
           System.out.println(b);
       }
        System.out.println("Prog Ended");

    }
}
