package ExceptionHandling;

import java.net.MalformedURLException;
import java.net.URL;

public class ExceptionDemo8 {
    public static void main(String[] args) {
        String link = "https://www.google.com//imghp";
        try {
            URL url = new URL(link);
            System.out.println(url.getHost());
            System.out.println(url.getPath());
            System.out.println(url.getProtocol());
        }
        catch (MalformedURLException a){
            System.out.println(a);
        }
    }
}
