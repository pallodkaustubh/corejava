package ExceptionHandling;

public class Exceptiondemo1 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        String s1 = "123abc";
        try{
            int no = Integer.parseInt(s1);
            System.out.println("no is : " +no);

        }
        catch (NumberFormatException n ){
            System.out.println(n);
        }
        System.out.println("Program Ended");
    }
}
