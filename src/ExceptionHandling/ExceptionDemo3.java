package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo3 {
    public static void main(String[] args) {
        System.out.println("Prog Started");
        Scanner sc1 = new Scanner(System.in);
        try{
            System.out.println("enter no 1");
            int n1 = sc1.nextInt();

            System.out.println("enter no 2");
            int n2= sc1.nextInt();

           double result =n1/n2;

            System.out.println(result);

        }
        catch(ArithmeticException a){
            System.out.println(a);
        }
        catch(InputMismatchException e){
            System.out.println(e);
        }
        System.out.println("Prog Ended");
    }
}
