package ExceptionHandling;

public class ExceptionDemo5 {
    public static void main(String[] args) {
        Object [] data = new String[2];
        try {
            data[0]="java";
            data[1]=50;
            System.out.println(data[0] + "\t" +data[1]);
        }
        catch (ArrayStoreException a){
            System.out.println(a);
        }
    }
}
