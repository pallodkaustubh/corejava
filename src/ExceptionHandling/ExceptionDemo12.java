package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo12 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Email Id");
        String email = sc1.next();
        validate(email);
    }
    static void validate(String email){
        if(email.contains("@") && email.contains("."))
        {
            System.out.println("Valid Email Id");
        }
        else {
            throw new InvalidEmailException("Invalid Email");
        }
    }

}
