package ExceptionHandling;

public class ExceptionDemo10 {
    public static void main(String[] args) {
        System.out.println("Main Started");
        info();
        System.out.println("Main Ended");
    }
    static void test(){
        System.out.println("Test Started");
        int c =10/0;    //Exception Origin
        System.out.println("Test Ended");
    }
    static void Display(){
        System.out.println("Display Started");
        try {
            test();
        }
        catch (ArithmeticException a)   //Exception Propagated
        {
            System.out.println(a);
        }

        System.out.println("Display Ended");
    }

    static void  info () {
        System.out.println("Info Started");
        Display();
        System.out.println("Info Ended");
    }
}
