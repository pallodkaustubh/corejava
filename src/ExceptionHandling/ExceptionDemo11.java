package ExceptionHandling;

import java.io.IOException;

public class ExceptionDemo11 {
    public static void main(String[] args) {
        System.out.println("Main Started");
        try {
            test();
        }
        catch (IOException a)
        {
            System.out.println(a);
        }
        System.out.println("Main Ended");
    }

    static void test() throws IOException {
        throw new IOException();
    }
}
