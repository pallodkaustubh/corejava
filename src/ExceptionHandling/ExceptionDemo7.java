package ExceptionHandling;

import java.io.FileWriter;
import java.io.IOException;

public class ExceptionDemo7 {
    public static void main(String[] args) {
        String msg = "java is oop language";

        FileWriter fw = null;
        try {
            fw = new FileWriter("D:\\core java\\abc.txt");
            fw.write(msg);
            fw.close();
            System.out.println("File created");
        }
        catch (IOException a){
            System.out.println(a);
        }
    }
}
