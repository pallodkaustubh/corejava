package ExceptionHandling;

public class ExceptionDemo2 {
    public static void main(String[] args) {

        //GENERIC Exception Handling

        System.out.println("Prog Started");
        String str = null;

        try {
            System.out.println(str.toLowerCase());
        }
        catch ( Exception a){
            System.out.println(a);
        }
        System.out.println("Prog Ended");
    }
}
