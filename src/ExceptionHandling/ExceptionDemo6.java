package ExceptionHandling;

public class ExceptionDemo6 {
    public static void main(String[] args) {
        System.out.println("program started");
        String str = "java";
        try {
            System.out.println("try started");
            System.out.println(str.length());
            System.out.println("try ended");

        } catch (NumberFormatException a) {
            System.out.println(a);
        }
        finally {
            System.out.println("Closing costly resources");
        }
        System.out.println("program ended");
    }
}
