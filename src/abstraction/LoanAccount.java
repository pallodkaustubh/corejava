package abstraction;

public class LoanAccount implements  Account {

    double LoanAmmount;

    public LoanAccount(double AccountBalance){
        this.LoanAmmount=AccountBalance;
        System.out.println("Loan acc created ");
    }

    @Override
    public void deposit(double amt) {
        LoanAmmount-=amt;
        System.out.println(amt+"rs debited to your account");
    }

    @Override
    public void withdraw(double amt) {
        LoanAmmount+=amt;
        System.out.println(amt+"rs credited to your account");

    }

    @Override
    public void checkBalance() {
        System.out.println("Active loan amt is" +LoanAmmount);
    }
}
