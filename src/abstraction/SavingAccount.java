package abstraction;

public class SavingAccount implements  Account {
    double AccountBalance;

    public SavingAccount(double AccountBalance){
        this.AccountBalance=AccountBalance;
        System.out.println("saving acc created ");
    }


    @Override
    public void deposit(double amt) {
        AccountBalance+=amt;
        System.out.println(amt+"rs credited to your account");
    }

    @Override
    public void withdraw(double amt) {
      if (AccountBalance>amt)
      {
          AccountBalance-=amt;
          System.out.println(amt+"rs debited from your account");
      }
      else
          System.out.println("insufficient balance");
    }

    @Override
    public void checkBalance() {
        System.out.println("active balance is "+AccountBalance);
    }
}
