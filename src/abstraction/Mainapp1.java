package abstraction;

import java.util.Scanner;

public class Mainapp1 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Select Account type ");
        System.out.println("1.Savings \n 2. Loan");
        int acctype= sc1.nextInt();
        System.out.println("Enter your acc opening balance");
        double balance= sc1.nextDouble();


        AccountFactory factory = new AccountFactory();
        Account accref = factory.createAccount(acctype,balance);
        boolean status = true;

        while (status)
        {
            System.out.println("select mode of transaction");
            System.out.println("1.Deposit\n2>Withdraw\n3.Check balance\4exit");
            int choice= sc1.nextInt();
            if (choice==1)
            {
                System.out.println("Enter Ammount");
                double amt = sc1.nextDouble();
                accref.deposit(amt);

            }
           else if (choice==2)
            {
                System.out.println("Enter Ammount");
                double amt = sc1.nextDouble();
                accref.withdraw(amt);

            } else if (choice==3)
            {
                accref.checkBalance();
            }
           else
            {
                status=false;
            }
        }
    }
}
