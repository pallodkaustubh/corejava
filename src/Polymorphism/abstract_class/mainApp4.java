package Polymorphism.abstract_class;

import java.util.Scanner;

public class mainApp4 {
    public static void main(String[] args) {
        //Heirarchial inheritance

        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select type");
        System.out.println("1 : MANAGER\n2 : WATCHMAN");
        int choice=sc1.nextInt();
        Employee c =null;
        if (choice==1)
        {
            c = new manger();
        }
        else if(choice==2)
        {
            c = new watchman();
        }
        c.getDesignation();
        c.getSalary();


        }
    }

