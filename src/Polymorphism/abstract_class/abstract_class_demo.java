package Polymorphism.abstract_class;

public abstract class abstract_class_demo {
    //static and nonstatic variables //
    static  int k =20;
    double d= 35.25;


    //non-static abstract and concrete methods //

    static void test() {
    }


    void display(){
        System.out.println("Display Method");
    }

    //static concrete method //

    abstract_class_demo(){
        System.out.println("Constructor");
    }

    //static and non-static block //

    static {
        System.out.println("Static Block");
    }

    {
        System.out.println("Non Static Block");
    }
}
