package Polymorphism.interface_;

public class MasterCard implements creditCard{
    @Override
    public void getType() {
        System.out.println("Credit Card Type is Mastercard");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction Success of $ : "+amt);
    }
}
