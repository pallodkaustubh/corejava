package Polymorphism.interface_;

public class visa implements creditCard{
    @Override
    public void getType() {
        System.out.println("Credit Card Type is Visa");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction Success of RS : "+amt);
    }
}
