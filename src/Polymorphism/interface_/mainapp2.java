package Polymorphism.interface_;

public class mainapp2 {
    public static void main(String[] args) {
        creditCard credit;
        credit=new visa();  //upcasting
        credit.getType();
        credit.withdraw(2500);
        System.out.println("===========================================");
        credit=new MasterCard();  //upcasting
        credit.getType();
        credit.withdraw(250);
    }
}
