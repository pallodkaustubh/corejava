package Polymorphism.interface_;

public interface interface_demo {
    //public static final variable
    int k=20;

    //non-static abstract method
    void  test();

    //static concrete method
    static void info(){
        System.out.println("Info Method");
    }

}
