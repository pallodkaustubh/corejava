package business_logic_class;

class Sample // Business Class
{
	 int k =10;
	 void test()
	{
		System.out.println("TEST METHOD");
	}

}

class MainApp2 // Executable Class
{
	public static void main(String[] args) 
	{
		System.out.println("k value is :"+ new Sample().k);
		new Sample().test();
	}
}