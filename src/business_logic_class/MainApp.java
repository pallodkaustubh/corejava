package business_logic_class;
//    Assignment No 1.    //


import java.util.Scanner;
 

public class MainApp

{                                                
 
    public static void main(String args[]) 

    {
 
        Scanner scanner = new Scanner(System.in);
        System.out.println("Find Largest Salary and Smallest Salary of three numbers");
 
        System.out.println("Please enter first Sal :");
        int first = scanner.nextInt();
 
        System.out.println("Please enter second Sal :");
        int second = scanner.nextInt();
 
        System.out.println("Please enter third Sal :");
        int third = scanner.nextInt();
 
        int largest = largest(first, second, third);
        int smallest = smallest(first, second, third);

        int difference = largest - smallest;
        System.out.println("Difference between LargeSal and  SmallSal: " +difference);


        // difference between  Largest  salary and Smallest Salary
 
        System.out.println("largest of three numbers  : " + largest);
        System.out.println("smallest of three numbers  : "+ smallest);
 
    
    }
 
    
    public static int largest(int first, int second, int third) {
        int max = first;
        if (second > max) {
            max = second;
        }
 
        if (third > max)
        {
            max = third;
        }
 
        return max;
    }
 
   
    public static int smallest(int first, int second, int third) 
    {
        int min = first;
        if (second < min) {
            min = second;
        }
 
        if (third < min) {
            min = third;
        }
 
        return min;
    }
}