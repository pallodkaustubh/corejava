package business_logic_class;

import java.util.Scanner;
class MainApp3
{
	public static void main(String[] args) 
	{
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Enter radius");
		double rad = sc1.nextDouble();

		System.out.println("Enter Your Choice");
		System.out.println("1. Area \n 2. Circumference");
		int Choice = sc1.nextInt();


		if (Choice==1)
		{
           //call area 
			new Circle().Area(rad);
		}
		else if (Choice==2)
		{
           //call circumference
			new Circle().Circumference(rad);
		} 	
		else
		{
			System.out.println("Invalid Choice");
		}	
		  

	}
}

class Circle
 
 {
 	static double pi = 3.14;

 	void Area(double rad)
 	{

 	double area = pi*rad*rad;

 	System.out.println("Area of Circle is "+area);

    }

    void Circumference(double rad)

   {

    double  Circum = 2*pi*rad;

    System.out.println("Circumference of Circle is: " +Circum);
   }


 }