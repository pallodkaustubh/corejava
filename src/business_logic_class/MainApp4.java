package business_logic_class;

import java.util.Scanner;
class Electricity
{
	static double residentialunit = 4.5;
	static double commercialunit = 8.5;

	void residentialbill(double current , double previous)
	{
		double totalunits = current - previous;
		double totalbill = totalunits*residentialunit;

		System.out.println("total bill is :"+totalbill);
	}

	void commercialbill(double current , double previous)
	{
		double totalunits = current - previous;
		double totalbill = totalunits*commercialunit;

		System.out.println("total bill is :"+totalbill);
	}

}

class MainApp4
{
	public static void main(String[] args) 
	{
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Enter current Reading :");
		double current = sc1.nextDouble();
		System.out.println("Enter previous Reading");
		double previous = sc1.nextDouble();

		System.out.println("Enter Cust type");
		
		System.out.println("1:Commercial \n 2: residential");
		int choice = sc1.nextInt();


		if (choice==1)
		{
			new Electricity().commercialbill(current,previous);

		}
		else if (choice==2)
		{
			new Electricity().residentialbill(current,previous);
		}
		else
		{
			System.out.println("Invalid Choice");

		}

	}
}