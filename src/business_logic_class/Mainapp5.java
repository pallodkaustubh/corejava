package business_logic_class;

class Book //business logic class
{
	String bookname;
	double bookprice;

	void acceptdetails(String name , double price)
	{
		bookname = name ;
		bookprice = price ;

	}
     
    void displaydetails()
    {
    	System.out.println("bookname :"+bookname);
    	System.out.println("bookprice :"+bookprice);
    }

}

class MainApp5
{
	public static void main(String[] args) 
	{
		Book b1 = new Book();

		b1.acceptdetails("java",2500);
		b1.acceptdetails("java",3500);
		b1.displaydetails();

		Book b2 = new Book();
		b2.acceptdetails("j2ee",2500.25);
		b2.displaydetails();
		
	}
}