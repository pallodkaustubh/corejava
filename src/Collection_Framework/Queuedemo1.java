package Collection_Framework;

import java.util.LinkedList;
import java.util.Queue;

public class Queuedemo1 {
    public static void main(String[] args) {
        Queue<Integer>data = new LinkedList<>();

        data.offer(25);
        data.offer(35);
        data.offer(40);
        System.out.println(data);

        System.out.println("Head of Queue is : "+data.peek());
        data.poll();

        System.out.println("Head of Queue is : "+data.peek());
        data.poll();
        System.out.println("Head of Queue is : "+data.peek());
        data.poll();
        System.out.println("Head of Queue is : "+data.peek());




    }
}
