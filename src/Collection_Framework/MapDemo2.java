package Collection_Framework;

import java.util.*;

public class MapDemo2 {
    public static void main(String[] args) {
        LinkedHashMap<Integer,String> data = new LinkedHashMap<>();
        data.put(91,"India");
        data.put(92,"PAK");
        data.put(93,"AFG");
        data.put(94,"China");


        //option 1
        System.out.println(data);

        //display all keys

        Set<Integer> keys = data.keySet();
        System.out.println(keys);


        //display all values
        Collection<String> info =data.values();
        System.out.println(info);

        //display all key value pairs

        Set<Map.Entry<Integer,String> > entries = data.entrySet();

        System.out.println("CODE\t\t\tCountry");
        System.out.println("+++++++++++++++++++++");
        for (Map.Entry<Integer,String> m :entries)
        {
            System.out.println(m.getKey()+"\t\t\t"+m.getValue());
        }
    }
}
