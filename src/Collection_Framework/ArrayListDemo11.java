package Collection_Framework;

import java.util.Scanner;

public class ArrayListDemo11 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        Bookoperation b1 = new Bookoperation();
        boolean status=true;
        while (status)
        {
            System.out.println("Select Mode of Operation");
            System.out.println("1:ADD New Book ");
            System.out.println("2:Display All Books");
            System.out.println("3:Delete Book");
            System.out.println("4:Exit");

            int choice = sc1.nextInt();

            switch (choice){
                case 1 :
                    System.out.println("Enter Book Id");
                    int id = sc1.nextInt();
                    System.out.println("Enter Book Name");
                    String name = sc1.next();

                    System.out.println("Enter book Price");
                    double price = sc1.nextDouble();

                    b1.addBook(id,name,price);

                    break;

                case 2 :
                    b1.displayBook();
                    break;
                case 3 :
                    System.out.println("Enter Book Id");
                    int BookId =sc1.nextInt();
                    b1.deleteBook(BookId);
                    break;
                case 4 :
                    status=false;
                    break;
            }
        }
    }
}
