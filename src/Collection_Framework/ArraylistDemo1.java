package Collection_Framework;

import java.util.ArrayList;

public class ArraylistDemo1 {
    public static void main(String[] args) {
        ArrayList data = new ArrayList();
        data.add(35);
        data.add("java");
        data.add(35.25);
        data.add('a');
        data.add(null);
        data.add(35);
        data.add(true);


        System.out.println(data);
    }
}
