package Collection_Framework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class ArraylistDemo3 {
    public static void main(String[] args) {
        ArrayList <String> data = new ArrayList<>();

        data.add("java");
        data.add("Sql");
        data.add("J2ee");
        data.add("Manual");

        //options to print the Arraylist

        //option 1

        System.out.println(data);

        //option2

        for (int a =0;a<data.size();a++)
        {
            System.out.print(data.get(a) + " ");
        }

        System.out.println();

        //option3

        for(String a : data)
        {
            System.out.print(a + " ");
        }
        System.out.println();
        //option 4
        Iterator<String> itr = data.iterator();
        if (itr.hasNext()){
            System.out.println(data);
        }


        //option5

        ListIterator<String> itr1 = data.listIterator();
        if (itr.hasNext())
        {
            System.out.println(data);
        }


    }

}
