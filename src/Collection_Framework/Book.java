package Collection_Framework;

public class Book {
    int bookId;
    String bookName;
    double bookPrice;

    public Book(int BookId,String BookName , double BookPrice){
        this.bookId = BookId;
        this.bookName = BookName;
        this.bookPrice = BookPrice ;
    }
    public  String toString() {
      return  bookId +"\t\t" + bookName + "\t\t" + bookPrice;

    }
}
