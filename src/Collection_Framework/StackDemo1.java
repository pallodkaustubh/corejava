package Collection_Framework;

import java.util.Stack;

public class StackDemo1 {
    public static void main(String[] args) {
        Stack<Integer> data = new Stack<>();
        data.push(25);           //bottom
        data.push(35);
        data.push(56);
        data.push(45);           //top

        System.out.println(data);

        System.out.println("Top of stack is  : " + data.peek());

        data.pop();            //25 will delete
        System.out.println("Top of stack is  : " + data.peek());

    }
}
