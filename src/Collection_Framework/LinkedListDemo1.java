package Collection_Framework;

import java.util.LinkedList;

public class LinkedListDemo1 {
    public static  void main(String[] args) {
        LinkedList<String> data = new LinkedList<>();

        data.add("Sql");
        data.add("Java");
        data.add("Manual");
        data.addFirst("Web Tech");
        data.removeLast();
        System.out.println(data);


    }
}
