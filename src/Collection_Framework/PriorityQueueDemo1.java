package Collection_Framework;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo1 {
    public static void main(String[] args) {
       PriorityQueue<Integer> data = new PriorityQueue<>();
        data.offer(96);
        data.offer(12);
        data.offer(64);
        System.out.println(data);
    }
}
