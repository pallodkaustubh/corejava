package Collection_Framework;

import java.util.ArrayList;

public class ArraylistDemo8 {
    public static void main(String[] args) {
        ArrayList<Integer> data1 = new ArrayList<>();
        ArrayList<Integer> data2 = new ArrayList<>();

        data1.add(25);
        data1.add(35);

        data2.add(65);
        data2.add(55);

        data2.addAll(data1);
        System.out.println(data2);
    }
}
