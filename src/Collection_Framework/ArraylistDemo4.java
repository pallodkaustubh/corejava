package Collection_Framework;

import java.util.ArrayList;
import java.util.Iterator;

public class ArraylistDemo4 {
    public static void main(String[] args) {
        ArrayList <Integer> data = new ArrayList<>();
        data.add(10);
        data.add(35);
        data.add(55);

        Iterator<Integer> itr = data.iterator();
                while(itr.hasNext()){
                    if (itr.next()==55)     ///fetch
                    {
                        itr.remove();       ///delete
                    }
                }
        System.out.println(data);

    }

}
