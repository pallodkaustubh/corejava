package Collection_Framework;

import java.util.ArrayList;
import java.util.Iterator;

public class Bookoperation {
    ArrayList<Book> booklist = new ArrayList<>();

    public  void  addBook (int id , String name , double price)
    {
        Book b1 = new Book(id,name,price);
        booklist.add(b1);
        System.out.println("Book Added");
    }

    public  void  displayBook(){
        System.out.println("Id\t\tName\t\tPrice");
        System.out.println("=======================");

        for (Book b : booklist)
        {
            System.out.println(b);
        }
    }

    public  void  deleteBook(int BookId){
        Iterator <Book> itr = booklist.iterator();

        while (itr.hasNext()){
            if (itr.next().bookId==BookId)
            {
                itr.remove();
                System.out.println("Book Removed ");
            }
        }

    }
}
