package Collection_Framework;

import java.util.ArrayList;

public class ArraylistDemo2 {
    public static void main(String[] args) {

        //Generic Arraylist
        ArrayList <Integer>  data = new ArrayList<>();

        data.add(25);
        data.add(35);
        data.add(45);

        System.out.println(data);

    }
}
