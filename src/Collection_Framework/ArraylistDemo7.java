package Collection_Framework;

import java.util.ArrayList;
import java.util.Scanner;

public class ArraylistDemo7 {
    public static void main(String[] args) {

        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Start Point");
        int n1 = sc1.nextInt();

        System.out.println("Enter End Point");
        int n2 = sc1.nextInt();


        ArrayList <Integer> even = new ArrayList<>();
        ArrayList <Integer> odd =new ArrayList<>();

        for(int i = n1 ; i < n2 ; i++)
        {
            if(i%2==0)
            {
                even.add(i);
            }
            else {
                odd.add(i);
            }
        }

        System.out.println("Even Nos  : " + even);
        System.out.println("Odd Nos : " + odd);
    }
}
