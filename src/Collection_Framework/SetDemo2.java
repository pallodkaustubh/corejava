package Collection_Framework;

import java.util.Scanner;

public class SetDemo2 {
    public static void main(String[] args) {
        ProductOperations p1 = new ProductOperations();
        Scanner sc1 = new Scanner(System.in);

        boolean status = true ;

        while (status)
        {
            System.out.println("Enter Mode of Ooerations");
            System.out.println("1.Display products\n2:Add to Cart\n3:Display Cart Items\n4:Remove From Cart\n5:Exit");
            int choice = sc1.nextInt();

            if (choice==1)
            {
                p1.displayProducts();
            }
            else if (choice==2)
            {
                System.out.println("Enter name of Product");
                String name = sc1.next();
                p1.addToCart(name);
            }
            else if (choice==3)
            {
                p1.displayCartItems();

            }
          else if (choice==4)
            {
                System.out.println("Enter Product To be Removed");
                String name = sc1.next();
                p1.removeProducts(name);
            }
          else if (choice==5){
                status=false;
            }

        }
    }
}
