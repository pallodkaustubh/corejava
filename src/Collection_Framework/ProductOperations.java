package Collection_Framework;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class ProductOperations {
    HashSet<String> products = new HashSet<>();
    LinkedHashSet<String> cartItems = new LinkedHashSet<>();

    //non-static block
    {
        products.add("TV");
        products.add("Fridge");
        products.add("AC");
        products.add("Laptop");

    }

    void displayProducts() {
        for (String s : products) {
            System.out.println(s);
        }
    }

    void addToCart(String name) {
        if (products.contains(name)) {
            cartItems.add(name);
            System.out.println("Added Successfully");
        } else {
            System.out.println("Product not found");

        }
    }

    void  displayCartItems(){
        for (String s: cartItems)
            System.out.println(s);
    }

    void  removeProducts(String name)
    {
        if(cartItems.contains(name)){
            cartItems.remove(name);
            System.out.println("Removed Successfully");
        }
        else {
            System.out.println("First Add To Cart");
        }
    }
}


