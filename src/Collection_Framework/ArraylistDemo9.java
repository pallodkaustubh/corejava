package Collection_Framework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ArraylistDemo9 {
    public static void main(String[] args) {
        ArrayList<Double> values = new ArrayList<>();

        values.add(95.45);
        values.add(5.91);
        values.add(63.21);

        //sorting in asc order

        Collections.sort(values);
        System.out.println(values);



        //sorting in desc order

        Collections.sort(values,Collections.reverseOrder());
        System.out.println(values);
    }
}
