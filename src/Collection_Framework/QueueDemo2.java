package Collection_Framework;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class QueueDemo2 {
    Queue<Integer> tokens = new LinkedList<>();
    static  int tokenNo = 1;

    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        QueueDemo2 d1 = new QueueDemo2();
        boolean status = true;
        while (status)
        {
            System.out.println("Select Mode of Operation");
            System.out.println("1:Generate Token\n2:Process Token\n3:Exit");

            int choice = sc1.nextInt();

            if (choice==1)
            {
                d1.generateToken();
            }
            else if (choice==2)
            {
                d1.processToken();

            }

            else {
                status=false;
            }
        }
    }

     void processToken() {

         System.out.println("Processing Token NO " +tokens.peek());
         tokens.poll();
         System.out.println("Process Completed");
    }

    void generateToken() {

        tokens.offer(tokenNo);
        System.out.println("Token NO is :"+tokenNo);
        tokenNo++;

    }
}
