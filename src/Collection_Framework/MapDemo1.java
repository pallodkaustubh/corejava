package Collection_Framework;

import java.util.HashMap;

public class MapDemo1 {
    public static void main(String[] args) {
        HashMap<Integer,String> data = new HashMap<>();
        data.put(12,"Sql");
        data.put(15,"Java");
        data.put(20,"J2EE");
        System.out.println(data);

    }
}
