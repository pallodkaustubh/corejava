package Collection_Framework;

import java.util.ArrayList;

public class ArraylistDemo10 {
    public static void main(String[] args) {
     product p1 = new product(1,2500,"TV");
     product p2 = new product(2,3500,"Fridge");
     product p3 = new product(3,4500,"Mobile");


        ArrayList<product>data = new ArrayList<>();
        data.add(p1);
        data.add(p2);
        data.add(p3);

        for (product p : data)
        {
            System.out.println(p);
        }

    }
}
