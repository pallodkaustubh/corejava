package break_continue;

import java.util.Scanner;

public class breakdemo2 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("choose language");
        System.out.println("1:java\n2:python\n3:php");
        int choice = sc1.nextInt();
        switch (choice)
        {
            case 1 :
                System.out.println("selected java");
                break;
            case 2 :
                System.out.println("selected python");
                break;
            case 3 :
                System.out.println("selected php");
                break;
            default :
                System.out.println("invalid choice");

        }
    }
}
