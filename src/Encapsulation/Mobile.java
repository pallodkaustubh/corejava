package Encapsulation;

public class Mobile {
    String company = "Samsung";

    //inner class
    class Ram{
        void displayInfo(){
            System.out.println("Ram Size is : 8 GB");
        }
    }

    //inner class

    class Processor{

        void displayProc(){
            System.out.println("Processor is : SnapDragon 625");
        }
    }
}
