package Encapsulation;

public class GoogleAccount {

    //static
    int count;

    static GoogleAccount g1;
    private GoogleAccount (){

    }

    //singleton method

    static GoogleAccount login(){
        if(g1 == null){
            //singleton obj
            g1 = new GoogleAccount();
            System.out.println("login success");
        }
        else {
            System.out.println("Already logged in");
        }
        return g1;
    }
    void acessGmail(){
        System.out.println("Accessing Gmail");
    }
    void acessDrive(){
        System.out.println("Accessing Drive");
    }
}
