package Encapsulation;

public class MainApp4 {
    public static void main(String[] args) {
        GoogleAccount acc1 = GoogleAccount.login();
        acc1.acessGmail();

        GoogleAccount acc2 = GoogleAccount.login();
        acc2.acessDrive();
    }
}
