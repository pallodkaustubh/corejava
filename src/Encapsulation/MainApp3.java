package Encapsulation;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter the radius");
        int r = sc1.nextInt();
        Shape s1 = new Shape() {
            @Override
            public void area() {
                double result = 3.14*r*r;
                System.out.println("Area of circle is :"+result);
            }
        };
        s1.area();

        Shape s2 = new Shape() {
            @Override
            public void area() {
                double result = 0.5*r*r;
                System.out.println("Area of Triangle is :"+result);
            }
        };
        s2.area();
    }
}
