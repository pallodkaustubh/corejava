package casting;

public class Mainapp5 {
    public static void main(String[] args) {
        bike b = new electricbike(); //upcasting
        b.getType();

        System.out.println("+++++++++++++++++++++++++++");

        electricbike e = (electricbike) b; //downcasting
        e.getType();
        e.batteryInfo();
    }
}
