package casting;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select service provider");
        System.out.println("1 : AirAsia \n 2: Indigo");
        int choice = sc1.nextInt();
        System.out.println("Select Route");
        System.out.println("0-pune-delhi");
        System.out.println("1-mum-chennai");
        System.out.println("2-kol-blr");
        int routeChoice = sc1.nextInt();

        System.out.println("Enter no of tickets ");
        int tickets = sc1.nextInt();
        goibibo g1=null;
        if(choice==1)
        {
            g1=new Airasia();

        } else if (choice==2)
        {
            g1=new indigo();

        }
        g1.bookTicket(tickets,routeChoice);
    }
}
