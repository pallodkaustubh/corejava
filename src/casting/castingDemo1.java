package casting;

public class castingDemo1 {
    public static void main(String[] args) {
        int a=15;
        double b =25.15;
        System.out.println(a + " " + b);
        int c = (int) 35.85; //narrowing
        double d = 26;
        System.out.println(c + "  " + d);  //widening

    }
}
