package casting;

public class laptop extends  machine{
    @Override
    void getType() {
        System.out.println("Machine Type is Laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        //15%gst added to total
        double total = qty*price;
        double Finalamt = total*0.15;
        System.out.println("FINAL AMT IS = "+Finalamt);
    }
}
