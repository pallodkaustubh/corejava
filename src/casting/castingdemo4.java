package casting;

public class castingdemo4 {
    public static void main(String[] args) {
        short s1=(short) 123456; //narrowing
        System.out.println(s1);
        long l1 = 846785512;
        int x1 = (int) l1 ;   //narrowing
        System.out.println(x1);
    }
}
