package casting;

import java.util.Scanner;

public class Mainapp4 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty = sc1.nextInt();
        System.out.println("Enter Price");
        double price = sc1.nextDouble();
        System.out.println("Select Machine type");
        System.out.println("1 : laptop \n 2: projector");
        int choice = sc1.nextInt();

         machine m1 =null;
        if(choice==1)
        {
            m1=new laptop();

        } else if (choice==2)
        {
            m1=new projector();

        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
}
