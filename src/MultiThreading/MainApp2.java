package MultiThreading;

public class MainApp2 {
    public static void main(String[] args) {
        demo2 d1 = new demo2();
        demo2 d2 = new demo2();

        Thread t1 = new Thread(d1);   //1st thread
        Thread t2 = new Thread(d2);   //2nd thread

        t1.start();
        t2.start();
    }
}
