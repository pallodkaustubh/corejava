package MultiThreading;

public class MainApp1 {
    public static void main(String[] args) {
        demo1 d1 = new demo1();   //thread1
        demo1 d2 = new demo1();   //thread2
        demo1 d3 = new demo1();   //thread3

        d1.start();
        d2.start();
        d3.start();
    }
}
