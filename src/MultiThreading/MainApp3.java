package MultiThreading;

public class MainApp3 {
    public static void main(String[] args) throws InterruptedException {
        Master m1 = new Master();
        Master m2 = new Master();
        Master m3 = new Master();

        m1.start();
        m2.start();
        m3.start();

        m1.join();
        m2.join();
        m3.join();
    }
}
