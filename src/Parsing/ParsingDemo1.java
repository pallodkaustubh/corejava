package Parsing;

public class ParsingDemo1 {
    public static void main(String[] args) {
        String s1 = "35";
        String s2 ="45";

        System.out.println(s1+s2);

        //parsing

        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);
        System.out.println(n1+n2);

        double d1=35.45;
        double d2 = 45.25;
        System.out.println(d1+d2);

        String s3 = Double.toString(d1);
        String s4 = Double.toString(d2);
        System.out.println(s3+s4);
    }
}
