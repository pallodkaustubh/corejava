package Method_overriding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter qty");
        int qty = sc1.nextInt();
        System.out.println("Enter price ");
        double price = sc1.nextDouble();
        System.out.println("Select E-commerce platform");
        System.out.println("1 : Amazon\n2 : Flipkart");
        int choice= sc1.nextInt();
        if(choice==1)
        {
            amazon a = new amazon();
            a.SellProduct(qty,price);
        }
        else if (choice==2)
        {
            flipkart f = new flipkart();
            f.SellProduct(qty,price);
        }
        else
        {
            System.out.println("Invalid choice");
        }

    }
}
