package Method_overriding;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
       Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty = sc1.nextInt();

        System.out.println("Enter Price");
        double price = sc1.nextDouble();

        System.out.println("choose online platform");
        System.out.println("1 : Zomato\n2 : Swiggy");

        int choice = sc1.nextInt();

        if (choice==1)
        {
            zomato z= new zomato();
            z.orderMenu(qty,price);
        }
        else if (choice==2)
        {
            swiggy s = new swiggy();
            s.orderMenu(qty,price);
        }
        else {
            System.out.println( "INVALID CHOICE");
        }

    }
}
