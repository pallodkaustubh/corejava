/*
   5
   45
  345
 2345
12345
 */



package programing_batch.Patterns;

public class program17 {
    public static void main(String[] args) {
        int star=1;
        int line=5;
        int space=line-1;

        for (int i=0;i<line;i++)
        {
            int ch=1;
            for (int k =0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++)
            {
                System.out.print(ch++);

            }
            System.out.println();
            space--;
            star++;

        }
    }
}
