/*

3456
2345
1234
0123
123
23
3


 */



package programing_batch.Patterns;

public class program36 {
    public static void main(String[] args) {
        int line = 7;
        int star = 4;
        int ch = 3;
        for (int i = 0; i < line; i++) {
            int ch1 = ch;
            for (int j = 0; j < star; j++) {
                System.out.print(ch1++);
            }
            System.out.println();
            if (i <=2) {
                ch--;
                star++;
            } else
                ch++;
            star--;


        }

    }
}