/*
1	1	1	1
1	1	1	1
1	1	1	1
1	1	1	1
 */



package programing_batch.Patterns;

public class program15 {

    public static void main(String[] args) {
        int line=4;
        int column=4;
        int ch=1;

        for(int i=0;i<line;i++)
        {

            for(int j=0;j<column;j++)
            {
                System.out.print(ch +"\t");
                if(ch>7)
                    ch=1;
            }
            System.out.println(" ");
        }
    }
}
