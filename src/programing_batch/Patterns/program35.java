/*

3
32
321
3210
321
32
3

 */


package programing_batch.Patterns;

public class program35 {

    public static void main(String[] args) {
        int line=7;
        int star=1;

        for (int i=0;i<line;i++)
        {
          int  ch=3;
          for (int j=0;j<star;j++)
          {
              System.out.print(ch--);
          }
            System.out.println();
            if (i<=2)
                star++;
            else
                star--;

        }
    }
}

