/*


    A
   AB
  ABC
 ABCD
ABCDE
 ABCD
  ABC
   AB
    A



 */




package programing_batch.Patterns;

public class program19 {
    public static void main(String[] args) {
        int star=1;
        int line=9;
        int space=4;
        for (int i=0 ;i<line;i++) {
            char ch = 'A';
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch++);
            }

            System.out.println();
            if (i <= 3) {
                space--;
                star++;

            } else {
                star--;
                space++;

            }

        }

    }
}
