/*
QuickSort
 */
package programing_batch.Arrays.Quicksort;

import programing_batch.Arrays.RandomArray;

public class  program1 {
    public static int partition(int arr[], int l, int h) {
        int pivot = arr[h];
        int i = l - 1;

        for (int j = l; j < h; j++) {
            if (arr[j] < pivot) {
                i++;
                //swap
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        i++;
        int temp = arr[i];
        arr[i] = pivot;
        arr[h] = temp;
        return i;   //pivot index
    }

    public static void quickSort(int[] arr, int l, int h) {
        if (l < h) {
            int pidx = partition(arr, l, h);

            quickSort(arr, l, pidx - 1);
            quickSort(arr, pidx + 1, h);
        }
    }

    public static void main(String[] args) {
        int[] arr = RandomArray.RandomArr(5);
        for (int a : arr) {
            System.out.print(a + " ");

        }
        System.out.println("");


        quickSort(arr, 0, arr.length - 1);

        //print
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}