/*
check if duplicate element in array
 */
package programing_batch.Arrays;

import java.util.Random;

public class program14 {
    public static void main(String[] args) {
        Random rd = new Random();
        int[] arr = new int[6];
        for (int a=0;a< arr.length-1;a++) {
            arr[a] = rd.nextInt(6);
            System.out.print(arr[a] + " ");
        }
        System.out.println();
        System.out.println("Duplicate elements");
        for (int i=0; i<arr.length;i++) {
            for (int j=i+1; j<arr.length; j++) {
                if (arr[i] == arr[j] ) {
                    System.out.println(arr[j]);
                }
            }
        }
    }
}
