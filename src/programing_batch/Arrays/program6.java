/*
reverse the array without creating another array

i/p :1,2,3,4,5

o/p : 3 4 1 2 5


 */





package programing_batch.Arrays;

public class program6 {
    public static void main(String[] args) {
        int[]arr={1,2,3,4,5};
        int count= arr.length/2;

        for (int i =0;i< arr.length/2;i++)
        {
            int temp=arr[i];
            arr[i]=arr[count];
            arr[count]=temp;
            count++;

        }
        System.out.println();
        for (int a:arr)
        {
            System.out.print(a+" ");
        }


    }
}
