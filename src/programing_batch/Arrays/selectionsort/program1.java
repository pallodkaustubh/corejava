/*
Selection Sort
 */
package programing_batch.Arrays.selectionsort;

import programing_batch.Arrays.RandomArray;

import java.util.Arrays;
import java.util.Random;

public class program1 {
    public static void main(String[] args) {

        int[] arr = RandomArray.RandomArr(5);
        for (int a : arr) {
            System.out.print(a + " ");

        }
        System.out.println("");


        for (int i = 0; i < arr.length; i++) {
            int minidx = i;
            for (int j = i+1; j < arr.length; j++) {
                if (arr[j] < arr[minidx])
                    minidx = j;

            }
            int temp = arr[i];
            arr[i] = arr[minidx];
            arr[minidx] = temp;
        }
        for (int a : arr) {
            System.out.print(a + " ");
        }

    }

}
