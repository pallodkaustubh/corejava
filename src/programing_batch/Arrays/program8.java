/*
merging two arrays

  int []arr1={1,2,3,4};
        int []arr2={5,6,7,8};

   o/p : arr3={1,5,2,6,3,7,4,8}

 */

package programing_batch.Arrays;

import java.util.Arrays;

public class program8 {
    public static void main(String[] args) {
        int []arr1={1,2,3,4};
        int []arr2={5,6,7,8};
        int[]arr3=new int[arr1.length+ arr2.length];
        int idx=0;
        for (int a:arr1)
        {
            arr3[idx]=a;
            idx+=2;
        }
        idx=1;
        for (int a:arr2)
        {
            arr3[idx]=a;
            idx+=2;
        }
        System.out.println(Arrays.toString(arr3));

    }
    }


