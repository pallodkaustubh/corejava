/*
No of occurance of each character
 */
package programing_batch.Strings;

public class program6 {
    public static void main(String[] args) {
        String str = "Java Program";
        int count=0;
        for(int i=0;i<str.length();i++)
        {
            if (str.charAt(i)=='a')
            {
                count++;
            }
        }
        System.out.println(count);
    }
}
