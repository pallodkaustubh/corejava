/*
Convert first letter of each word in string to lowercase
 */
package programing_batch.Strings;

public class program8 {
    public static void main(String[] args) {
        String str ="THIS IS PROGRAMMING BATCH";

        String [] arr = str.split(" ",0);

        for( String s: arr)
        {

            String s2 = s.replace(s.charAt(0),s.toLowerCase().charAt(0));
            System.out.print(s2 + " ");
        }
    }
}
