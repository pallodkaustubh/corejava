package Constructor_overloading;

public class master {
    master(int a){
        System.out.println(a);
    }
    master(String s)
    {
        this(25);
        System.out.println(s);
    }
    master(char c)
    {
        this("java");
        System.out.println(c);

    }
}
