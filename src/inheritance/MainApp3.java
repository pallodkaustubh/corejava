package inheritance;

public class MainApp3 {
    public static void main(String[] args) {
        permEmployee p1=new permEmployee();
        p1.getInfo(101,35000);
        p1.getDesignation("Sr.engineer");

        System.out.println("=======================================================");

        contractEmployee c1=new contractEmployee();
        c1.getInfo(102,15000);
        c1.getContractDetails(12);

    }
}
