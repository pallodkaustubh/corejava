package forloop;

import java.util.Scanner;

public class forloopdemo5 {
    public static void main(String[] args) {
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("Enter start point");
        int a = Sc1.nextInt();
        System.out.println("Enter end point");
        int b = Sc1.nextInt();

        double sum = 0.0;

        for (int c=a;c<=b;c++) {
            if (c % 2 != 0) {
                sum = sum + c;
            }
        }
            System.out.println(sum);


    }
}
