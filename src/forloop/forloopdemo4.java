package forloop;

import java.util.Scanner;

public class forloopdemo4
{
    public static void main(String[] args) {
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("enter start point");
        int a = Sc1.nextInt();
        System.out.println("enter end point");
        int b = Sc1.nextInt();

        for(int c = b ; c>=a ; c--)
        {
           if(c%2!=0){
               System.out.println(c);

           }
        }
    }
}
