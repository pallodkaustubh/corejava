package FileHandling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandling_writing {
    public static void main(String[] args) {
        File file = new File("D:\\core java\\xyz.txt");

        try {
        FileWriter fw = new FileWriter(file.getAbsoluteFile());

        String txt = "This is my First File";

        fw.write(txt);
        fw.append("\t\t\t Hii " );
        fw.close();
        System.out.println("File Updated");


    } catch (
    IOException e) {
        throw new RuntimeException(e);
    }
    }
}
