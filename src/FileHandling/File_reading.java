package FileHandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class File_reading {
    public static void main(String[] args) {
        File file = new File("D:\\core java\\xyz.txt");

        try {
            FileReader fr = new FileReader(file.getAbsoluteFile());

            int i ;

            while ((i=fr.read())!=-1)
                System.out.print((char) i);
            fr.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
