package OBJECTS;

class mobile
{
	//state
	String company = "zebronics";
	double price = 2500;
	Boolean isnfcenabled=true;
	int ramsize = 8;


	//behaviour

	void calling()
	{
		System.out.println("make and recieve calls");

	}

	void installapss()
	{
		System.out.println("installing apps");
	}
}

class MainApp1
{
	public static void main(String[] args) 
	{
		mobile m1 = new mobile();
		//accessing state

		System.out.println("company " +m1.company);
		System.out.println("price "+m1.price);
		System.out.println("ramsize "+m1.ramsize);
		System.out.println("isnfcenabled "+m1.isnfcenabled);



		//accessing behaviour

		m1.calling();
		m1.installapss();
	}
}