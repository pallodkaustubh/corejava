package OBJECTS;

import java.util.Scanner;
class MainApp3
{
	public static void main(String[] args) 
	{
		ticket t1 = new ticket();
		Scanner sc1 = new Scanner(System.in);

		System.out.println("1.book");
		System.out.println("2.cancel");
		System.out.println("3.show");

		int choice = sc1.nextInt();


		if(choice==1)
		{
			System.out.println("enter no of tickets to be booked");
			int count = sc1.nextInt();
			//call bookticket method

			t1.bookingTicket(count);
		}
		else if(choice==2)
		{
			System.out.println("enter no of tickets to be cancelled");
			int count = sc1.nextInt();
			//call cancelticket method

			t1.cancelTicket(count);
		}
		else if(choice==3)
		{
			
			//call show ticket method

			t1.showticket();
		}
		else 
		{
			System.out.println("invalid choice");
		}
		
		}


}


class ticket
{
	static int availableticket = 50;
	static double ticketcost = 150;

	void bookingTicket(int bookingcount)
	{
      availableticket = availableticket + bookingcount;
      double totalamt = bookingcount*ticketcost;
      System.out.println("total ammount to be paid is "+totalamt);
      System.out.println(bookingcount + "tickets have been booked");

      showticket();
	}



	void cancelTicket(int cancelcount)
	{
      availableticket = availableticket - cancelcount;
      
      
      System.out.println(cancelcount +  "tickets have been cancelled");

      showticket();
	}


	void showticket()
	{
      System.out.println("available ticket count is  "+availableticket);
    }
}