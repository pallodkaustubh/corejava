package blocks;

import java.util.Scanner;

class atmSimulator
{
	static Scanner sc1 = new Scanner(System.in);
	static double accountBalance =50000.25;
	static int pin;


	static
	{
		System.out.println("select language");
		System.out.println("1:English\t2:Marathi");
		int choice = sc1.nextInt();
		if(choice==1)
		{
			System.out.println("selected english");
		}
		else if(choice==2)
		{
			System.out.println("selected marathi");
		} 
		else
		{
             System.exit(0);
		}
    }

    static
    {
         System.out.println("Enter pin");
         pin = sc1.nextInt();
    }

    public static void main(String[] args) 
    {
       if(pin==4595)
       {
          
          System.out.println("1:Withdraw\t 2:Check Balance\t 3:Change Pin");
    	  int Choice = sc1.nextInt();
    	  if (Choice==1)
    		{
    			System.out.println("Enter ammount");
    			double amt = sc1.nextDouble();
    			withdraw(amt);
    		}
    		else if (Choice==2) 
    		{

    			checkBalance();
    		
    	    }
    	    else if (Choice==3)
    	    {
    	    	System.out.println("Enter new pin");
    	    	int newpin = sc1.nextInt();
    	    	changePin(newpin);
    	    }

       }
    	    
    	  	

       else 
       {
       	System.out.println("invalid pin");
       }


       
    }

    static void withdraw(double amt)
    {
    	if(amt<=accountBalance)
    	{
    		accountBalance -=amt;
    		System.out.println(amt+ "is debited from account");
    	}
    	else 
    	{
    		System.out.println("insufficient funds");
    	}
    }

    static void checkBalance()
    {
    	System.out.println("account balance is :"+accountBalance);
    }

    static void changePin(int newpin)
    {
    	pin = newpin;
    	System.out.println("pin changed succesfully");
    }
  }