package Methods;

import java.util.Scanner;

class ControlDemo1

{
   public static void main(String[] args) 
   {
   	Scanner sc1 = new Scanner(System.in);
   	System.out.println("enter your age ");
   	int age = sc1.nextInt();

   	Validate(age);


   }

   static void Validate(int age)
   {
   	if(age>=18)
   	{
   		System.out.println("eligible for voting");

   	}
   	else 
   	{
   		System.out.println("not eligible for voting");

   	}
   }

}