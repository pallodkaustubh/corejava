package Methods;

import java.util.Scanner;

class ControlDemo3

{
	public static void main(String[] args) 
	{
	 Scanner sc1 = new Scanner(System.in);
	 System.out.println("enter num1");
	 int num1 = sc1.nextInt();
	 System.out.println("enter num2");
	 int num2 = sc1.nextInt();

	 Validate(num1,num2);

	}

	static void Validate(int num1, int num2)
	{
		if(num1>num2)
		{
			 System.out.println("num1 is greater than num2");

		}
		else if (num1<num2)
		{
			 System.out.println("num2 is greater than num1");

		}
		else 
		{
			 System.out.println("both are equal");
		}
	}
}