package Methods;

class MethodDemo5
{
	static void kmtom(double km)
	{
		double kmtom = km*1000;
		System.out.println("km to m :" +kmtom);
	}

	static void mtokm(double m)
	{
		double mtokm = m/1000;
		System.out.println("m to km is :"+mtokm);
	}

	public static void main (String[] args)
	{
		kmtom(10);
		mtokm(1000);
	}
}