package Methods;

class MethodDemo2
{
	static void addition(double a ,double b)
	{
	 double c= a+b;
	 System.out.println("Addition is : " +c);

	}

	public static void main (String[] args)
	 {
	   addition(10,20);
	   addition(10.25,40.25);
	 }
}