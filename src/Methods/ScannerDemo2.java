package Methods;

import java.util.Scanner;

class ScannerDemo2

{
	public static void main(String[] args) 
	{
		Scanner sc1 = new Scanner(System.in);
		System.out.println("enter total no of matches ");
		double total = sc1.nextDouble();
		System.out.println("enter no of matches won");
		double win = sc1.nextDouble();
		System.out.println("enter no of matches lost");
		double loss = sc1.nextDouble();
		System.out.println("enter no of matches drawn");
		double draw = sc1.nextDouble();

		calculate(total,win,loss,draw);



	}


	static void calculate(double t , double w , double l ,double d)
	{
		double points = (w*5)+(l*-2)+(d*3);

		double win_perc = (w/t)*100;

		System.out.println("points eraned in total are "+points);
		System.out.println("win percentage is "+win_perc);
	}
}