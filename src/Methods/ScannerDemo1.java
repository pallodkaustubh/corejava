package Methods;

import java.util.Scanner;
class ScannerDemo1
{
	public static void main(String[] args) 
	{
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Enter the number 1");
		int num1 = sc1.nextInt();

		System.out.println("Enter the number 2");
		int num2 = sc1.nextInt();

        addition(num1, num2);
        subtraction(num1,num2);
        division(num1 , num2);
        multiplication(num1 , num2);
	}
		
	static void addition(int num1,int num2)
	{
		int r = num1 + num2;
		System.out.println("the addition of two numbers is "+r);

	}


	static void subtraction(int num1,int num2)
	{
		int r = num1 - num2;
		System.out.println("the subtraction of two numbers is "+r);
		
	}

	static void division(double num1,double num2)
	{
		double r = num1 / num2;
		System.out.println("the division of two numbers is "+r);
		
	}
	static void multiplication(int num1,int num2)
	{
		int r = num1 * num2;
		System.out.println("the product of two numbers is "+r);
		
	}

}