package Methods;

public class MethodDemo6 {
    public static void main(String[] args)
    {
        incrementsalary();
    }

    static double calculatesalary(double basic , double hra)
    {

        double total = basic+hra;
        return total;
    }
    

    static void incrementsalary()
    {
        double var = calculatesalary(10000,2500);

        double inc_salary = var + var * 0.25;
        System.out.println("Incremented salary is :" +inc_salary);
    }

}

