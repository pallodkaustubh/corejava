package democalculator;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calculator extends JFrame implements ActionListener {



    JPanel panel1,panel2;
    JTextField txtOutput;
    double first,second,result;
    String operator;
    Calculator()

    {
        panel1 = new JPanel();
        panel2 = new JPanel();
        txtOutput= new JTextField(25);
        txtOutput.setFont(new Font(Font.SANS_SERIF ,Font.PLAIN,14));
        txtOutput.setPreferredSize(new Dimension(25,30));
        panel1.add(txtOutput);
        panel2.setLayout(new GridLayout(4, 4,10,10));
        int n=1;

        for(int i=1;i<=16;i++)
        {
            String no="" +n;
            if (i==4) no = "+";
            else if(i==8) no ="-";
            else if(i==12) no = "*";
            else if(i==13) no = "0";
            else if(i==14) no = ".";
            else if(i==15) no = "=";
            else if(i==16) no = "/";
            else
                n++;
            JButton b = new JButton(no);
            panel2.add(b);
            b.addActionListener(this);


        }
        add(panel1, BorderLayout.NORTH);
        add(panel2);
        setSize(300,300);
        setVisible(true);
        setResizable(false
        );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
    public void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        String answer = "";
        if (command=="+" || command == "-" || command=="*" || command == "/")
        {
            first = Double.parseDouble(txtOutput.getText());
            operator=command;
            txtOutput.setText("");
        }
        else if(command=="=")
        {
            second = Double.parseDouble(txtOutput.getText());
            switch (operator)
            {
                case "+":
                {
                    result=first+second;
                    break;
                }
                case "+-":
                {
                    result=first-second;
                    break;
                }
                case "*":
                {
                    result=first*second;
                    break;
                }
                case "/":
                {
                    result=first/second;
                    break;
                }

            }
            txtOutput.setText(result + "");
            operator="=";
        }
        else
        {
            if(operator=="=")
            {
                txtOutput.setText("");
                operator="";
            }
            txtOutput.setText(txtOutput.getText().concat(command));
        }
    }
    public static void main(String args[])
    {
        new Calculator();
    }
}
